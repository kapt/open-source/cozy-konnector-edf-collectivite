const {
  BaseKonnector,
  requestFactory,
  log,
  errors
} = require('cozy-konnector-libs')
const { format, parse } = require('date-fns')
const slugify = require('cozy-slug')
const sleep = require('util').promisify(global.setTimeout)

// Global constants
const VENDOR = 'EDF Collectivité'
const BASE_URL = 'https://entreprises-collectivites.edf.fr/'
const AUTH_BASE_URL = 'https://auth.entreprises-collectivites.edf.fr/'

// Requests constants
const cookieJar = requestFactory().jar()
const userAgent =
  'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'

let auraRequestNumber = -1 // A global counter to get the request number to insert in all the requests to https://entreprises-collectivites.edf.fr/espaceclientpremium/s/sfsites/aura endpoint. Requests must start à 0 and increment by 1 for each request.
let auraConfig = null // A global variable to store the aura config, which is retrieved once and then used in all the requests to https://entreprises-collectivites.edf.fr/espaceclientpremium/s/sfsites/aura endpoint

let o11ySecondaryLoader = null // A global variable to store the o11ySecondaryLoader, which is retrieved once and then used in all the POSTAuraRequest requests having inclO11ySecondaryLoader set to true

let dashboardURL = null // A global variable to store the URL of the dashboard. If the account is registered on the premium dashboard, the URL will be /espaceclientpremium/, otherwise /espaceclient/. The kind of dashboard is identified when the user is logged in.

// Billing account. Ex: 5297292430
let billingAccount = null

// Date to retrieve invoices from
let fromDate = null

// Date to retrieve invoices to
let toDate = null

const APIDatesFormat = 'yyyy/MM/dd'

// Initialize different kinds of requests

const requestParams = {
  // The debug mode shows all the details about HTTP requests and responses. Very useful for
  // debugging but very verbose. This is why it is commented out by default
  debug: false,

  // If cheerio is activated do not forget to deactivate json parsing (which is activated by
  // default in cozy-konnector-libs
  json: false,

  // This allows request-promise to keep cookies between requests
  jar: cookieJar,

  headers: {
    'User-Agent': userAgent,
    'Accept-Language': 'fr-FR,fr;q=0.9'
  },

  strictSSL: false, // These days there is an error with the SSL certificate of EDF website. This is a workaround, waiting for a fix of the certificate setup on their side.

  // Inject full response on requests, allowing to check for response status code
  resolveWithFullResponse: true
}

const docRequestAccept =
  'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8'

const XHRAccept = 'application/json, text/javascript, */*; q=0.01'

// A [request](https://github.com/request/request-promise) instance, with cheerio and for document requests
const cheerioDocRequest = requestFactory({
  ...requestParams,

  headers: { ...requestParams.headers, Accept: docRequestAccept },

  // Activates [cheerio](https://cheerio.js.org/) parsing on each page
  cheerio: true
})

// A [request](https://github.com/request/request-promise) instance, without cheerio and for document requests
const rawDocRequest = requestFactory({
  ...requestParams,

  headers: { ...requestParams.headers, Accept: docRequestAccept },

  // Des-activate [cheerio](https://cheerio.js.org/) parsing
  cheerio: false
})

// A [request](https://github.com/request/request-promise) instance, without cheerio, with .
const rawXHRRequest = requestFactory({
  ...requestParams,

  json: true,

  headers: {
    ...requestParams.headers,
    Accept: XHRAccept,
    'Content-Type': 'application/json',
    'accept-api-version': 'protocol=1.0,resource=2.0'
  },

  // Des-activate [cheerio](https://cheerio.js.org/) parsing
  cheerio: false
})

/*
A wrapper around request-promise requests to perform a GET request, checking the status code of the request.

Args:
  - uri: The URL of the request to perform
  - qs: The query params
  - type: The type of request to perform. Can be "Doc" or "XHR"
  - cheerio: Indicates if cheerio should be used to parse the response

Returns:
- The full request
*/
async function GETRequest(uri, qs = {}, type = 'Doc', cheerio = true) {
  // log('debug', `GET request to : ${uri}`)
  // log('debug', 'with query : ')
  // log('debug', qs)
  // log('debug', 'of type : ' + type)
  // log('debug', 'with cheerio : ' + cheerio)

  let fullResponse

  if (type === 'Doc') {
    if (cheerio) {
      fullResponse = await cheerioDocRequest({
        uri: uri,
        qs: qs
      })
    } else {
      fullResponse = await rawDocRequest({
        uri: uri,
        qs: qs
      })
    }
  } else if (type === 'XHR') {
    fullResponse = await rawXHRRequest({
      uri: uri,
      qs: qs
    })
  }

  // Sleep for 1 sec between each requests to avoid being blocked by EDF
  await sleep(1000)

  const statusCode = fullResponse.statusCode

  if (statusCode !== 200) {
    throw new Error(errors.VENDOR_DOWN)
  }

  return fullResponse
}

/*
A wrapper around request-promise requests to perform a POST request, checking the status code of the request.

Args:
  - uri: The URL of the request to perform
  - data: The data to POST
  - type: The type of request to perform. Can be "Doc" or "XHR"
  - cheerio: Indicates if cheerio should be used to parse the response

Returns:
- The full request
*/
async function POSTRequest(uri, data, type = 'Doc', cheerio = true) {
  // log('debug', `POST request to : ${uri}`)
  // log('debug', 'with data : ')
  // log('debug', data)
  // log('debug', 'of type : ' + type)
  // log('debug', 'with cheerio : ' + cheerio)

  let fullResponse

  if (type === 'Doc') {
    if (cheerio) {
      fullResponse = await cheerioDocRequest({
        uri: uri,
        method: 'POST',
        form: data
      })
    } else {
      fullResponse = await rawDocRequest({
        uri: uri,
        method: 'POST',
        form: data
      })
    }
  } else if (type === 'XHR') {
    fullResponse = await rawXHRRequest({
      uri: uri,
      method: 'POST',
      body: data
    })
  }

  // Sleep for 1 sec between each requests to avoid being blocked by EDF
  await sleep(1000)

  const statusCode = fullResponse.statusCode

  if (statusCode !== 200) {
    log('debug', statusCode, 'error')
    throw new Error(errors.VENDOR_DOWN)
  }

  return fullResponse
}

function getAuraRequestNumber() {
  auraRequestNumber++
  return auraRequestNumber
}

/*
A wrapper around POSTRequest to perform a POST request to the Aura API, building the request body

Args:
  - endpointURL: The URL to post the data to. Must include a pattern like "/{requestNumber}/" that will be replaced in the function by the request number/. Ex: https://entreprises-collectivites.edf.fr/espaceclientpremium/s/sfsites/aura?r={requestNumber}&ui-comm-runtime-components-aura-components-siteforce-controller.PubliclyCacheableComponentLoader.getPageComponent=1

  - messageActions: An array of actions, like :
      [
            {
                "id": "27;a",
                "descriptor": "aura://ComponentController/ACTION$getComponent",
                "callingDescriptor": "UNKNOWN",
                "params": {
                    "name": "markup://instrumentation:o11ySecondaryLoader",
                    "attributes": {
                        "aura:id": "o11ySecondaryLoader",
                        "accessible": true,
                    },
                },
            },
        ]

    - pageURI: the value of aura.pageURI in the requestBody. Ex: /espaceclientpremium/s/

    - inclO11ySecondaryLoader (optional): Indicates if the COMPONENT@markup://instrumentation:o11ySecondaryLoader must be set in the requestBody context "loaded". Default: false

    - inclObjectHome (optional): Indicates if the COMPONENT@markup://forceCommunity:objectHome must be set in the requestBody context "loaded". Default: false

Returns:
- The full request
*/
async function POSTAuraRequest(
  endpointURL,
  messageActions,
  pageURI,
  inclO11ySecondaryLoader = false,
  inclObjectHome = false
) {
  const contextLoaded = {
    'APPLICATION@markup://siteforce:communityApp':
      auraConfig.context.loaded['APPLICATION@markup://siteforce:communityApp']
  }

  if (inclO11ySecondaryLoader) {
    contextLoaded['COMPONENT@markup://instrumentation:o11ySecondaryLoader'] =
      o11ySecondaryLoader
  }

  if (inclObjectHome) {
    contextLoaded['COMPONENT@markup://forceCommunity:objectHome'] =
      '2ypE1qN24e3H4qotJN-b-Q' // TODO: Fill this if you know from where this value comes
  }

  const requestBody = {
    message: {
      actions: messageActions
    },
    'aura.context': {
      mode: 'PROD',
      fwuid: auraConfig.context.fwuid,
      app: 'siteforce:communityApp',
      loaded: contextLoaded,
      dn: [],
      globals: {},
      uad: false
    },
    'aura.pageURI': pageURI,
    'aura.token': auraConfig.token
  }

  requestBody['message'] = JSON.stringify(requestBody['message'])
  requestBody['aura.context'] = JSON.stringify(requestBody['aura.context'])

  const requestNumber = getAuraRequestNumber()

  const requestURL = endpointURL.replace('{requestNumber}', requestNumber)

  const response = await POSTRequest(requestURL, requestBody, 'Doc', false)
  // log('debug', response.body)

  return response
}

module.exports = new BaseKonnector(start)

async function start(fields) {
  // Initialize user input values in global vars
  initInputValues(fields)
  // log("debug", fields)

  // Login to create a session on the server with authenticate()
  await authenticate(fields)

  // Get global Aura parameters, then used in all requests
  await getAuraParameters()

  // Check that the billing account is valid.
  // The check must be performed on the whole list of perimeters, because the billing account can be valid for some perimeters and not for others.
  let communitiesFound = false

  if (dashboardURL === 'espaceclientpremium') {
    // Get perimeters
    const perimeters = await getPerimeters()

    for (const perimeter of perimeters) {
      selectPerimeter(perimeter)

      // Get a list of communities to run the konnector on
      let communities = await getCommunities()

      if (communities.length > 0) {
        communitiesFound = true
      }

      // Save the invoices of the filtered communities
      await saveCommunitiesInvoices.bind(this)(fields, communities)
    }
  } else {
    // Get a list of communities to run the konnector on
    let communities = await getCommunities()

    if (communities.length > 0) {
      communitiesFound = true
    }

    // Save the invoices of the filtered communities
    await saveCommunitiesInvoices.bind(this)(fields, communities)
  }

  if (communitiesFound === false) {
    throw new Error(
      `Invalid billing account "${billingAccount}", please check your input parameters`
    )
  }

  // Disconnect / clear session on the backend
  await disconnect()
}

function initInputValues(fields) {
  if (fields['contractNumber']) {
    billingAccount = fields['contractNumber']
  }

  const today = new Date(Date.now())

  if (fields['fromDate']) {
    fromDate = parse(fields['fromDate'], 'yyyy-MM-dd', new Date())
    fromDate = format(fromDate, APIDatesFormat)
  } else {
    fromDate = parseInt(today.getFullYear(), 10) - 3 + '/01/01'
  }

  if (fields['toDate']) {
    toDate = parse(fields['toDate'], 'yyyy-MM-dd', new Date())
    toDate = format(toDate, APIDatesFormat)
  } else {
    toDate = format(today, APIDatesFormat)
  }
}

async function authenticate(fields) {
  log('info', 'Connecting to the provider website...')

  // Check that the credentials are set properly
  if (!fields.login) {
    log(
      'error',
      'The account is not correctly configured, the login field is missing'
    )
    throw new Error(errors.LOGIN_FAILED)
  }

  if (!fields.password) {
    log(
      'error',
      'The account is not correctly configured, the password field is missing'
    )
    throw new Error(errors.LOGIN_FAILED)
  }

  try {
    // A first request must be performed so that the mandatory cookies are set on the client
    const firstLoginPage = await GETRequest(`${BASE_URL}/espaceclient/s/`)
    // log("debug", firstLoginPage)

    // Extract the script tag that contains a redirect in JavaScript. We retrieve the URL and redirect the client
    const firstLoginPageScript = firstLoginPage.body('script').html()
    // log("debug", firstLoginPageScript)

    // There is a redirect in JavaScript, we retrieve the URL and redirect the client
    const firstRedirectUrl = firstLoginPageScript.match(
      /window.location.href ='(.*)'/
    )[1]
    // log("debug", firstRedirectUrl)

    const XUILoginPage = await GETRequest(firstRedirectUrl, {}, 'Doc', false)

    /*
      firstRedirectUrl is an url like: https://entreprises-collectivites.edf.fr/espaceclient/services/auth/sso/CNICE?startURL=%2Fespaceclient%2F%2Fespaceclient%2Fs%2F

      From this URL, the server redirects the client to several URLs in 302:
      - #1 redirect: https://auth.entreprises-collectivites.edf.fr/openam/oauth2/authorize?response_type=code&client_id=SalesforceCommunity&redirect_uri=https%3A%2F%2Fentreprises-collectivites.edf.fr%2Fespaceclient%2Fservices%2Fauthcallback%2FCNICE&scope=openid+profile&state=ABCDEF , with ABCDEF an unique token

      - #2 redirect: https://auth.entreprises-collectivites.edf.fr/openam/UI/Login?realm=%2Ffront_office&goto=https%3A%2F%2Fauth.entreprises-collectivites.edf.fr%2Fopenam%2Foauth2%2Fauthorize%3Fresponse_type%3Dcode%26client_id%3DSalesforceCommunity%26redirect_uri%3Dhttps%253A%252F%252Fentreprises-collectivites.edf.fr%252Fespaceclient%252Fservices%252Fauthcallback%252FCNICE%26scope%3Dopenid%2520profile%26state%3DABCDEF , with ABCDEF the same token as in #1

      - #3 redirect (final): https://auth.entreprises-collectivites.edf.fr/openam/XUI/#login/&realm=%2Ffront_office&goto=https%3A%2F%2Fauth.entreprises-collectivites.edf.fr%2Fopenam%2Foauth2%2Fauthorize%3Fresponse_type%3Dcode%26client_id%3DSalesforceCommunity%26redirect_uri%3Dhttps%253A%252F%252Fentreprises-collectivites.edf.fr%252Fespaceclient%252Fservices%252Fauthcallback%252FCNICE%26scope%3Dopenid%2520profile%26state%3DABCDEF , with ABCDEF the same token as in #1

      XUILoginPage contains the result of the #3 redirect request.
      The page contains a form loaded in JS that must be submitted with login and password to authenticate on the backend.

      When loaded, the JS extracts (and removes the hash in history with something like history.replaceState) the GET parameters, and then makes a POST request to the URL:

      https://auth.entreprises-collectivites.edf.fr/openam/json/authenticate?realm=/front_office&goto=https%3A%2F%2Fauth.entreprises-collectivites.edf.fr%2Fopenam%2Foauth2%2Fauthorize%3Fresponse_type%3Dcode%26client_id%3DSalesforceCommunity%26redirect_uri%3Dhttps%253A%252F%252Fentreprises-collectivites.edf.fr%252Fespaceclient%252Fservices%252Fauthcallback%252FCNICE%26scope%3Dopenid%2520profile%26state%3DABCDEF, with ABCDEF the same token as in #1

      So, we must extract the hash from the URL, and then make a POST request to the URL with the hash as a GET parameter.

    */
    let XUIPageHash = XUILoginPage.req.res.request.uri.hash
    // log("debug", XUIPageHash)

    const XUIPageQS = XUIPageHash.replace('#login/&', '')
    // log("debug", XUIPageQS)

    const authPageURI = `${AUTH_BASE_URL}openam/json/authenticate?${XUIPageQS}`
    // log("debug", authPageURI)

    const authenticateDataResponse = await POSTRequest(
      authPageURI,
      null,
      'XHR',
      false
    )
    // log("debug", authenticateDataResponse)

    const authenticateData = authenticateDataResponse.body
    // log("debug", authenticateData)

    for (const callback of authenticateData['callbacks']) {
      if (callback['type'] === 'NameCallback') {
        for (const input of callback['input']) {
          if (input['name'] === 'IDToken1') {
            input['value'] = fields.login
          }
        }
      }
      if (callback['type'] === 'PasswordCallback') {
        for (const input of callback['input']) {
          if (input['name'] === 'IDToken2') {
            input['value'] = fields.password
          }
        }
      }
    }

    // eslint-disable-next-line no-unused-vars
    const authenticateResponse = await POSTRequest(
      authPageURI,
      authenticateData,
      'XHR',
      false
    )
    // log("debug", authenticateResponse)

    // -> At this point, the backend has return a token and ask the client to assign its value to a cookie "ICESSOsession"

    // POST on https://auth.entreprises-collectivites.edf.fr/openam/json/users?_action=idFromSession&realm=/front_office
    // eslint-disable-next-line no-unused-vars
    const JSONUsersGetIDResponse = await POSTRequest(
      `${AUTH_BASE_URL}openam/json/users/?_action=idFromSession&realm=/front_office`,
      null,
      'XHR',
      false
    )
    // log("debug", JSONUsersGetIDResponse.body)

    // Extract oauth2 authorize URL from the XUIPageQS hash
    // log("debug", XUIPageQS)
    const oauth2AuthorizeURLParams = new URLSearchParams(XUIPageQS)
    const oauth2AuthorizeGoTo = decodeURIComponent(
      oauth2AuthorizeURLParams.get('goto')
    )
    // log("debug", oauth2AuthorizeGoTo)

    // eslint-disable-next-line no-unused-vars
    let espacesSPage = await GETRequest(oauth2AuthorizeGoTo)
    // log("debug", espacesSPage.body.html())

    /*
    oauth2AuthorizeGoTo contains link like:
    https://auth.entreprises-collectivites.edf.fr/openam/oauth2/authorize?response_type=code&client_id=SalesforceCommunity&redirect_uri=https%3A%2F%2Fentreprises-collectivites.edf.fr%2Fespaceclient%2Fservices%2Fauthcallback%2FCNICE&scope=openid%20profile&state=ABCDEF where ABCDEF is the same token as in #1

    From this URL, the server redirects the client to several URLs in 302:

    - #1 redirect: https://entreprises-collectivites.edf.fr/espaceclient/services/authcallback/CNICE?code=UUID4&scope=openid%20profile&iss=https%3A%2F%2Fauth.entreprises-collectivites.edf.fr%3A443%2Fopenam%2Foauth2&state=ABCDEF&client_id=SalesforceCommunity where ABCDEF is the same token as in #1 and UUID4 is a random token

    - #2 redirect: https://entreprises-collectivites.edf.fr/espaces?ErrorCode=NO_ACCESS&ErrorDescription=User+was+not+authorized+for+the+community&ProviderId=AZERTY&startURL=%2Fespaceclient%2F%2Fespaceclient%2Fs%2F, with AZERTY the provider identifier

    - #3 redirect (final): https://entreprises-collectivites.edf.fr/espaces/s/error?ErrorCode=NO_ACCESS&ErrorDescription=User%20was%20not%20authorized%20for%20the%20community&ProviderId=AZERTY&startURL=%2Fespaceclient%2Fs%2F , with AZERTY the provider identifier

      espacesSPage contains the result of the #3 redirect request.
    */

    // Then, the user must be redirected in GET to https://entreprises-collectivites.edf.fr/espaces/s/
    espacesSPage = await GETRequest(`${BASE_URL}espaces/s/`)

    // Extract the script tag that contains a redirect in JavaScript. We retrieve the URL and redirect the client
    const espacesSPageScript = espacesSPage.body('script').html()
    // log("debug", espacesSPageScript)

    const espacesSRedirectUrl = espacesSPageScript.match(
      /window.location.href ='(.*)'/
    )[1]
    // log("debug", espacesSRedirectUrl)

    /*
    espacesSRedirectUrl : an URL like : https://entreprises-collectivites.edf.fr/espaces/services/auth/sso/CNICE?startURL=%2Fespaces%2Fs%2F
    */
    const ssoCNICEPage = await GETRequest(espacesSRedirectUrl, {}, 'Doc', true)
    // log("debug", ssoCNICEPage.body)

    // Extract the script tag that contains a redirect in JavaScript. We retrieve the URL and redirect the client
    const ssoCNICEPageScript = ssoCNICEPage.body('script').html()
    // log("debug", ssoCNICEPageScript)

    const ssoCNICERedirectUrl = ssoCNICEPageScript.match(
      /window.location.href ='(.*)'/
    )[1]
    // log("debug", ssoCNICERedirectUrl)

    /*
      ssoCNICERedirectUrl: an URL like : https://entreprises-collectivites.edf.fr/espaces/secur/frontdoor.jsp?sid=AAAAA&retURL=%2Fespaces%2Fs%2F&apv=1&allp=1&untethered=&cshc=BBBBB&refURL=https%3A%2F%2Fentreprises-collectivites.edf.fr%2Fespaces%2Fsecur%2Ffrontdoor.jsp&fromSsoFlow=1

      From this URL, we load the frontdoorPage
    */

    const frontdoorPage = await GETRequest(ssoCNICERedirectUrl, {}, 'Doc', true)
    // log("debug", frontdoorPage.body.html())

    // Extract the script tag that contains a redirect in JavaScript. We retrieve the URL and redirect the client
    const frontdoorPageBody = frontdoorPage.body.html()
    // log("debug", frontdoorPageBody)

    const frontdoorRedirectUrl = frontdoorPageBody.match(
      /window.location.href ='(.*)'/
    )[1]
    // log("debug", frontdoorRedirectUrl)

    /*
      frontdoorRedirectUrl: an URL like : https://entreprises-collectivites.edf.fr/espaces/apex/CNICE_VFP234_EPIAiguillage?retURL=%2Fespaces%2Fs%2F

      From this URL, we load the apexCNICEPage
    */
    const apexCNICEPage = await GETRequest(
      frontdoorRedirectUrl,
      {},
      'Doc',
      true
    )
    // log("debug", apexCNICEPage.body.html())

    // Extract the script tag that contains a redirect in JavaScript. We retrieve the URL and redirect the client
    const apexCNICEPageBody = apexCNICEPage.body.html()
    // log("debug", apexCNICEPageBody)

    let apexCNICERedirectUrl = apexCNICEPageBody.match(
      /window.location.href ='(.*)'/
    )[1]
    apexCNICERedirectUrl = apexCNICERedirectUrl.substring(
      1,
      apexCNICERedirectUrl.length
    )
    apexCNICERedirectUrl = `${BASE_URL}${apexCNICERedirectUrl}`
    // log("debug", apexCNICERedirectUrl)

    // eslint-disable-next-line no-unused-vars
    const espacesCNICEPage = await GETRequest(
      apexCNICERedirectUrl,
      {},
      'Doc',
      true
    )
    // log("debug", espacesCNICEPage.body.html())

    // Check if the login was successful

    const espacesCNICEPageBody = espacesCNICEPage.body.html()

    if (espacesCNICEPageBody.includes('/espaceclientpremium')) {
      dashboardURL = 'espaceclientpremium'
    } else {
      dashboardURL = 'espaceclient'
    }
    const extranetHomePageURL = `${BASE_URL}${dashboardURL}/s/`

    const extranetHomePage = await GETRequest(extranetHomePageURL)
    // log('debug', extranetHomePage.body.html())

    const extranetHomePageScript = extranetHomePage.body('script').html()

    const isLogged = !extranetHomePageScript.includes('redirectOnLoad')
    if (!isLogged) {
      throw new Error(errors.LOGIN_FAILED)
    }

    log('info', '✓ Connection established !')

    log('info', 'Extracting aura config from home page JS...')
    const extranetHomePageContent = extranetHomePage.body.html()
    auraConfig = JSON.parse(
      extranetHomePageContent.match(/auraConfig = ({.*})/)[1]
    )
    const cn = auraConfig['eikoocnekot']
    const token =
      cookieJar._jar.store.idx['entreprises-collectivites.edf.fr']['/'][cn]
        .value
    auraConfig['token'] = token
    // log('debug', auraConfig)

    log('info', '✓ Aura config extracted')
  } catch (err) {
    log('error', err.message)

    if (err.statusCode === 401) {
      throw new Error(errors.LOGIN_FAILED)
    } else {
      throw new Error(errors.VENDOR_DOWN)
    }
  }
}

async function getAuraParameters() {
  log('info', 'Getting extra Aura parameters from endpoints...')

  /* Here we get two types of parameters from two endpoints.
  Theses parameters are then used to built the aura.context of all the requests to the API.
  */

  /*
  Extract the first parameters in a auraParameters1Response var, calling an URL like:

  https://entreprises-collectivites.edf.fr/espaceclientpremium/s/sfsites/aura?r=0&ui-comm-runtime-components-aura-components-siteforce-controller.PubliclyCacheableComponentLoader.getPageComponent=1

  */

  let auraParameters1Actions

  if (dashboardURL === 'espaceclientpremium') {
    auraParameters1Actions = [
      {
        id: '2;a',
        descriptor:
          'serviceComponent://ui.comm.runtime.components.aura.components.siteforce.controller.PubliclyCacheableComponentLoaderController/ACTION$getPageComponent',
        callingDescriptor: 'UNKNOWN',
        params: {
          attributes: {
            viewId: '7afd2a80-7533-4e22-bcce-b7fde5ea4fb3', // This value can be fetch from https://entreprises-collectivites.edf.fr/espaceclientpremium/s/sfsites/l/ABCDEF/bootstrap.js?aura.attributes=GHIJKL (link is available in the https://entreprises-collectivites.edf.fr/espaceclientpremium/s/ source code), if needed
            routeType: 'home',
            themeLayoutType: 'Home',
            params: {
              viewid: 'fc8077fc-1bb1-485b-818c-d4de966cb8a7', // This value can be fetch from https://entreprises-collectivites.edf.fr/espaceclientpremium/s/sfsites/l/ABCDEF/bootstrap.js?aura.attributes=GHIJKL (link is available in the https://entreprises-collectivites.edf.fr/espaceclientpremium/s/ source code), if needed
              view_uddid: '',
              entity_name: '',
              audience_name: '',
              picasso_id: '',
              routeId: ''
            },
            hasAttrVaringCmps: false,
            pageLoadType: 'STANDARD_PAGE_CONTENT',
            includeLayout: true
          },
          publishedChangelistNum: auraConfig.attributes.publishedChangelistNum,
          brandingSetId: auraConfig.attributes.brandingSetId
        }
      }
    ]
  } else {
    auraParameters1Actions = [
      {
        id: '2;a',
        descriptor:
          'serviceComponent://ui.comm.runtime.components.aura.components.siteforce.controller.PubliclyCacheableComponentLoaderController/ACTION$getPageComponent',
        callingDescriptor: 'UNKNOWN',
        params: {
          attributes: {
            viewId: '16a9f0bb-1e5b-4a34-8214-5255cb68dedd',
            routeType: 'home',
            themeLayoutType: 'Home',
            params: {
              viewid: '07ae883f-728b-47f5-b2e7-d3df903e6127',
              view_uddid: '',
              entity_name: '',
              audience_name: '',
              picasso_id: '',
              routeId: ''
            },
            hasAttrVaringCmps: false,
            pageLoadType: 'STANDARD_PAGE_CONTENT',
            includeLayout: true
          },
          publishedChangelistNum: 129,
          brandingSetId: '83fc8e1a-c7e2-4c61-a790-058df013c5b9'
        }
      }
    ]
  }

  // eslint-disable-next-line no-unused-vars
  const auraParameters1Response = await POSTAuraRequest(
    `${BASE_URL}${dashboardURL}/s/sfsites/aura?r={requestNumber}&ui-comm-runtime-components-aura-components-siteforce-controller.PubliclyCacheableComponentLoader.getPageComponent=1`,
    auraParameters1Actions,
    `/${dashboardURL}/s/`
  )
  // log('debug', auraParameters1Response.body)

  /*
  Extract the second parameters in a auraParameters2Response var, calling an URL like:

  https://entreprises-collectivites.edf.fr/espaceclientpremium/s/sfsites/aura?r=1&aura.ApexAction.execute=1&aura.Component.getComponent=1&ui-chatter-components-messages.Messages.getMessagingPermAndPref=1&ui-communities-components-aura-components-forceCommunity-navigationMenu.NavigationMenuDataProvider.getNavigationMenu=1&ui-force-components-controllers-hostConfig.HostConfig.getConfigData=1&ui-self-service-components-profileMenu.ProfileMenu.getProfileMenuResponse=1

  */

  let auraParameters2Actions
  if (dashboardURL === 'espaceclientpremium') {
    auraParameters2Actions = [
      {
        id: '27;a',
        descriptor: 'aura://ComponentController/ACTION$getComponent',
        callingDescriptor: 'UNKNOWN',
        params: {
          name: 'markup://instrumentation:o11ySecondaryLoader',
          attributes: {
            'aura:id': 'o11ySecondaryLoader',
            accessible: true
          }
        }
      },
      {
        id: '31;a',
        descriptor:
          'serviceComponent://ui.chatter.components.messages.MessagesController/ACTION$getMessagingPermAndPref',
        callingDescriptor: 'UNKNOWN',
        params: {},
        storable: true
      },
      {
        id: '63;a',
        descriptor:
          'serviceComponent://ui.self.service.components.profileMenu.ProfileMenuController/ACTION$getProfileMenuResponse',
        callingDescriptor: 'UNKNOWN',
        params: {}
      },
      {
        id: '91;a',
        descriptor:
          'serviceComponent://ui.communities.components.aura.components.forceCommunity.navigationMenu.NavigationMenuDataProviderController/ACTION$getNavigationMenu',
        callingDescriptor: 'markup://forceCommunity:navigationMenuBase',
        params: {
          navigationLinkSetIdOrName: '',
          includeImageUrl: false,
          addHomeMenuItem: true,
          menuItemTypesToSkip: ['SystemLink', 'Event', 'Modal'],
          masterLabel: 'Default Navigation'
        },
        version: '57.0',
        storable: true
      },
      {
        id: '118;a',
        descriptor:
          'serviceComponent://ui.force.components.controllers.hostConfig.HostConfigController/ACTION$getConfigData',
        callingDescriptor: 'UNKNOWN',
        params: {},
        storable: true
      },
      {
        id: '120;a',
        descriptor: 'aura://ApexActionController/ACTION$execute',
        callingDescriptor: 'UNKNOWN',
        params: {
          namespace: '',
          classname: 'CNICE_VFC147_CguController',
          method: 'isCguAccepted',
          cacheable: false,
          isContinuation: false
        }
      }
    ]
  } else {
    auraParameters2Actions = [
      {
        id: '28;a',
        descriptor: 'aura://ComponentController/ACTION$getComponent',
        callingDescriptor: 'UNKNOWN',
        params: {
          name: 'markup://instrumentation:o11ySecondaryLoader',
          attributes: { 'aura:id': 'o11ySecondaryLoader', accessible: true }
        }
      },
      {
        id: '32;a',
        descriptor:
          'serviceComponent://ui.chatter.components.messages.MessagesController/ACTION$getMessagingPermAndPref',
        callingDescriptor: 'UNKNOWN',
        params: {},
        storable: true
      },
      {
        id: '65;a',
        descriptor:
          'serviceComponent://ui.self.service.components.profileMenu.ProfileMenuController/ACTION$getProfileMenuResponse',
        callingDescriptor: 'UNKNOWN',
        params: {}
      },
      {
        id: '93;a',
        descriptor:
          'serviceComponent://ui.communities.components.aura.components.forceCommunity.navigationMenu.NavigationMenuDataProviderController/ACTION$getNavigationMenu',
        callingDescriptor: 'markup://forceCommunity:navigationMenuBase',
        params: {
          navigationLinkSetIdOrName: 'Default_Navigation',
          includeImageUrl: false,
          addHomeMenuItem: true,
          menuItemTypesToSkip: ['SystemLink', 'Event', 'Modal'],
          masterLabel: 'Default Navigation'
        },
        version: '61.0',
        storable: true
      },
      {
        id: '117;a',
        descriptor:
          'serviceComponent://ui.force.components.controllers.hostConfig.HostConfigController/ACTION$getConfigData',
        callingDescriptor: 'UNKNOWN',
        params: {},
        storable: true
      },
      {
        id: '119;a',
        descriptor: 'aura://ApexActionController/ACTION$execute',
        callingDescriptor: 'UNKNOWN',
        params: {
          namespace: '',
          classname: 'CNICE_VFC205_VosFactures',
          method: 'getBAsByContactId',
          cacheable: true,
          isContinuation: false
        }
      },
      {
        id: '120;a',
        descriptor: 'aura://ApexActionController/ACTION$execute',
        callingDescriptor: 'UNKNOWN',
        params: {
          namespace: '',
          classname: 'CNICE_VFC147_CguController',
          method: 'isCguAccepted',
          cacheable: false,
          isContinuation: false
        }
      },
      {
        id: '122;a',
        descriptor: 'aura://ComponentController/ACTION$getComponent',
        callingDescriptor: 'UNKNOWN',
        params: { name: 'markup://lightning:f6Controller', attributes: {} }
      }
    ]
  }

  const auraParameters2Response = await POSTAuraRequest(
    `${BASE_URL}${dashboardURL}/s/sfsites/aura?r={requestNumber}&aura.ApexAction.execute=1&aura.Component.getComponent=1&ui-chatter-components-messages.Messages.getMessagingPermAndPref=1&ui-communities-components-aura-components-forceCommunity-navigationMenu.NavigationMenuDataProvider.getNavigationMenu=1&ui-force-components-controllers-hostConfig.HostConfig.getConfigData=1&ui-self-service-components-profileMenu.ProfileMenu.getProfileMenuResponse=1`,
    auraParameters2Actions,
    `/${dashboardURL}/s/`
  )
  // log('debug', auraParameters2Response.body)

  o11ySecondaryLoader = auraParameters2Response.body.match(
    /"COMPONENT@markup:\/\/instrumentation:o11ySecondaryLoader":"(.+?)"}/
  )[1]
  // log('debug', o11ySecondaryLoader)
}

async function getPerimeters() {
  log('info', 'Fetching the list of perimeters...')

  const perimetersParameters = [
    {
      id: '63;a',
      descriptor: 'aura://ApexActionController/ACTION$execute',
      callingDescriptor: 'UNKNOWN',
      params: {
        namespace: '',
        classname: 'CNICE_VFC247_SelectPerim',
        method: 'getAllContactPerimeters',
        cacheable: true,
        isContinuation: false
      }
    },
    {
      id: '64;a',
      descriptor: 'aura://ApexActionController/ACTION$execute',
      callingDescriptor: 'UNKNOWN',
      params: {
        namespace: '',
        classname: 'CNICE_VFC247_SelectPerim',
        method: 'isGhostMode',
        cacheable: true,
        isContinuation: false
      }
    },
    {
      id: '65;a',
      descriptor: 'aura://ApexActionController/ACTION$execute',
      callingDescriptor: 'UNKNOWN',
      params: {
        namespace: '',
        classname: 'CNICE_VFC247_SelectPerim',
        method: 'getMarchesEligCollecte',
        cacheable: true,
        isContinuation: false
      }
    },
    {
      id: '66;a',
      descriptor: 'aura://ApexActionController/ACTION$execute',
      callingDescriptor: 'UNKNOWN',
      params: {
        namespace: '',
        classname: 'CNICE_VFC247_SelectPerim',
        method: 'getURLCollecte',
        cacheable: true,
        isContinuation: false
      }
    }
  ]

  const perimetersParametersResponse = await POSTAuraRequest(
    `${BASE_URL}espaceclientpremium/s/sfsites/aura?r={requestNumber}&aura.ApexAction.execute=4`,
    perimetersParameters,
    '/espaceclientpremium/s/aiguillage'
  )

  const perimetersParametersDetails = JSON.parse(
    perimetersParametersResponse.body
  )

  const perimeters =
    perimetersParametersDetails.actions[0].returnValue.returnValue.perimeters

  log(
    'info',
    `✓ Fetch perimeters: The konnector will be run on ${perimeters.length} perimeters`
  )

  return perimeters
}

async function selectPerimeter(perimeter) {
  log('info', 'Selecting the perimeter... ' + perimeter.perimeterName)

  const selectPerimeterParameters = [
    {
      id: '77;a',
      descriptor: 'aura://ApexActionController/ACTION$execute',
      callingDescriptor: 'UNKNOWN',
      params: {
        namespace: '',
        classname: 'CNICE_VFC247_SelectPerim',
        method: 'updatePerimeterInSession',
        params: {
          perimeterId: perimeter.perimeterId,
          perimeterRole: perimeter.role
        },
        cacheable: false,
        isContinuation: false
      }
    }
  ]

  // eslint-disable-next-line no-unused-vars
  const selectPerimeterResponse = await POSTAuraRequest(
    `${BASE_URL}espaceclientpremium/s/sfsites/aura?r={requestNumber}&aura.ApexAction.execute=1`,
    selectPerimeterParameters,
    '/espaceclientpremium/s/aiguillage'
  )
  // log('debug', selectPerimeterResponse.body)

  log('info', '✓ Select perimeter: ok')
}

async function getCommunities() {
  log('info', 'Fetching the list of communities...')

  const JSONCommunitiesActions = [
    {
      id: '308;a',
      descriptor: 'aura://ApexActionController/ACTION$execute',
      callingDescriptor: 'UNKNOWN',
      params: {
        namespace: '',
        classname: 'CNICE_VFC205_VosFactures',
        method: 'getBAsByContactId',
        params: { mapFilters: {} },
        cacheable: true,
        isContinuation: false
      }
    }
  ]

  const JSONCommunitiesResponse = await POSTAuraRequest(
    `${BASE_URL}${dashboardURL}/s/sfsites/aura?r={requestNumber}&aura.ApexAction.execute=1`,
    JSONCommunitiesActions,
    `/${dashboardURL}/s/factures`
  )
  // log('debug', JSONCommunitiesResponse.body)

  const JSONCommunitiesResponseBody = JSON.parse(JSONCommunitiesResponse.body)
  let communities = []
  for (const action of JSONCommunitiesResponseBody.actions) {
    if (action.returnValue.returnValue.bas) {
      communities = action.returnValue.returnValue.bas
    }
  }
  // log('debug', communities)

  if (!communities.length) {
    throw new Error('No communities found for these credentials')
  }

  communities = getCommunitiesAccounts(communities)

  log(
    'info',
    `✓ Fetch communities: The konnector will be run on ${communities.length} communities`
  )

  return communities
}

function getCommunitiesAccounts(communities) {
  let filteredCommunities = communities

  if (billingAccount) {
    filteredCommunities = filteredCommunities.filter(
      com => com.baFelixId === billingAccount
    )
  }

  return filteredCommunities
}

async function saveCommunitiesInvoices(fields, communities) {
  log('info', 'Saving invoices for the communities...')

  for (const community of communities) {
    await saveCommunityInvoices.bind(this)(fields, community)
  }

  log('info', '✓ Invoices saved for communities')
}

async function saveCommunityInvoices(fields, community) {
  const communityInvoicesListActions = {
    actions: [
      {
        id: '929;a',
        descriptor: 'aura://ApexActionController/ACTION$execute',
        callingDescriptor: 'UNKNOWN',
        params: {
          namespace: '',
          classname: 'CNICE_VFC260_ListeFactures',
          method: 'getListFactures',
          params: {
            startDate: fromDate,
            endDate: toDate,
            raes: [],
            cfs: [community.baMoeId],
            numFactures: [],
            idsFelix: [],
            montant: null
          },
          cacheable: false,
          isContinuation: false
        }
      }
    ]
  }

  // Alternative method to get the invoices list, doesn't allow to filter by date on the server side, keep it here for memory
  // const communityInvoicesListActions = [
  //     {
  //         "id": "342;a",
  //         "descriptor": "aura://ApexActionController/ACTION$execute",
  //         "callingDescriptor": "UNKNOWN",
  //         "params": {
  //             "namespace": "",
  //             "classname": "CNICE_VFC205_VosFactures",
  //             "method": "getFacturesbyCFMoeId",
  //             "params": {"moeId": community.baMoeId},
  //             "cacheable": false,
  //             "isContinuation": false,
  //         },
  //     }
  // ]

  const communityInvoicesListResponse = await POSTAuraRequest(
    `${BASE_URL}${dashboardURL}/s/sfsites/aura?r={requestNumber}&aura.ApexAction.execute=1`,
    communityInvoicesListActions,
    `/${dashboardURL}/s/factures`,
    true,
    true
  )
  // log('debug', communityInvoicesListResponse.body)

  const invoicesListDetails = JSON.parse(communityInvoicesListResponse.body)

  let invoices = []

  for (const action of invoicesListDetails.actions) {
    if (action.returnValue.returnValue.factures) {
      invoices = action.returnValue.returnValue.factures
    }
  }
  // log("debug", invoices)

  for (const invoice of invoices) {
    const invVendor = slugify(VENDOR)
    const invCommunityName = slugify(community.baName)
    const invCommunitySiret = community.siretChorus
    const invBusinessAccount = community.baMoeId
    const invBillingAccount = community.baFelixId
    const invLabel = invoice.num_facture
    const invDate = parse(invoice.date_emission, APIDatesFormat, new Date())
    const invDateFormat = format(invDate, 'ddMMyyyy')
    const invAmountInclTax = Number(invoice.montant_ttc)

    const filename = `${invVendor}_${invCommunityName}_${invCommunitySiret}_${invBusinessAccount}_${invBillingAccount}_${invLabel}_${invDateFormat}_${invAmountInclTax}EUR.pdf`

    const document = [
      {
        filename: filename,
        amount: invAmountInclTax,
        date: invDate,
        vendor: VENDOR,
        contractId: invBillingAccount,
        title: invLabel,
        currency: '€',
        fetchFile: fetchInvoice,
        invoice: invoice
      }
    ]

    await this.saveBills(document, fields, {
      fileIdAttributes: ['filename'],
      contentType: 'application/pdf',
      identifiers: ['date', 'amount', 'vendor'],
      sourceAccount: invCommunitySiret,
      sourceAccountIdentifier: invBillingAccount,
      timeout: Date.now() + 15 * 1000
    })
  }
}

async function fetchInvoice(document) {
  const invoice = document.invoice

  const downloadInvoiceActions = [
    {
      id: '342;a',
      descriptor: 'aura://ApexActionController/ACTION$execute',
      callingDescriptor: 'UNKNOWN',
      params: {
        namespace: '',
        classname: 'CNICE_VFC205_VosFactures',
        method: 'getFacturePdf',
        params: { factureId: invoice.num_facture },
        cacheable: false,
        isContinuation: false
      }
    }
  ]

  const downloadInvoiceResponse = await POSTAuraRequest(
    `${BASE_URL}${dashboardURL}/s/sfsites/aura?r={requestNumber}&aura.ApexAction.execute=1`,
    downloadInvoiceActions,
    `/${dashboardURL}/s/factures`,
    true,
    true
  )
  const invoiceDetails = JSON.parse(downloadInvoiceResponse.body)

  let fileContent = null
  for (const action of invoiceDetails.actions) {
    if (action.returnValue?.returnValue?.fileContent) {
      fileContent = action.returnValue.returnValue.fileContent
    }
  }

  if (!fileContent) {
    throw new Error(errors.FILE_DOWNLOAD_FAILED)
  }

  return new Buffer.from(fileContent, 'base64')
}

async function disconnect() {
  log('info', 'Disconnecting from the provider website...')

  try {
    const logoutPage = await GETRequest(
      `${BASE_URL}${dashboardURL}/secur/logout.jsp`
    )
    // log('info', logoutPage.body.html("script"))

    const redirectUrl = logoutPage.body
      .html('script')
      .match(/'(https:\/\/.*)'/)[1]
    // log('info', redirectUrl)

    // eslint-disable-next-line no-unused-vars
    const logoutRedirectPage = await GETRequest(redirectUrl)
    // log('info', logoutRedirectPage.body.html())

    // Check if disconnect was successful
    const extranetHomePage = await GETRequest(`${BASE_URL}/${dashboardURL}/s/`)
    // log('debug', extranetHomePage.body.html())

    const extranetHomePageScript = extranetHomePage.body('script').html()

    const isLogged = !extranetHomePageScript.includes('redirectOnLoad')

    if (isLogged) {
      throw new Error(errors.UNKNOWN_ERROR)
    }

    log('info', '✓ Disconnected')
  } catch (err) {
    log('error', err.message)
    throw new Error(errors.UNKNOWN_ERROR)
  }
}
