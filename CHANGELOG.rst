Changelog
=========


v1.0.2 (2023-09-13)
-------------------

Bug fixes
~~~~~~~~~
- Updates after EDF login process changed a little [Benjamin PIERRE]

Documentation
~~~~~~~~~~~~~
- Update changelog [Benjamin PIERRE]

Maintenance
~~~~~~~~~~~
- Update author in package.json [Benjamin PIERRE]


v1.0.1 (2023-03-16)
-------------------

Bug fixes
~~~~~~~~~
- Redefined var by mistake [Benjamin PIERRE]

Documentation
~~~~~~~~~~~~~
- Update changelog [Benjamin PIERRE]

Maintenance
~~~~~~~~~~~
- Fix webpack config [Benjamin PIERRE]


v1.0.0 (2023-03-16)
-------------------

Features
~~~~~~~~
- Re-write connector for 2023-03 version of EDF extranet [Benjamin
  PIERRE]

Documentation
~~~~~~~~~~~~~
- Update changelog [Benjamin PIERRE]


v0.0.4 (2022-11-21)
-------------------

Features
~~~~~~~~
- Removed unnecessary try/catch on start function, causing errors not to
  be raised properly [Benjamin PIERRE]

Documentation
~~~~~~~~~~~~~
- Update changelog [Benjamin PIERRE]

Maintenance
~~~~~~~~~~~
- Remove old reference to .travis.yml file [Benjamin PIERRE]


v0.0.3 (2022-02-09)
-------------------

Bug fixes
~~~~~~~~~
- Invalid syntax in gitlab-ci file [Benjamin PIERRE]
- Remove npm_package_repository_url var in package.json as it breaks
  with npm > 6 https://github.com/npm/cli/issues/2609 and not realy
  mandatory [Benjamin PIERRE]

Documentation
~~~~~~~~~~~~~
- Update changelog [Benjamin PIERRE]

Maintenance
~~~~~~~~~~~
- CI deploy stage only for new tags [Benjamin PIERRE]
- Add cozy publish in CI [Benjamin PIERRE]
- Remove debug infos in CI and reset SSH keys :p [Benjamin PIERRE]
- Debug CI #9 [Benjamin PIERRE]
- Debug CI #8 [Benjamin PIERRE]
- Debug CI #7 [Benjamin PIERRE]
- Debug CI #6 [Benjamin PIERRE]
- Debug gitlab ci #5 [Benjamin PIERRE]
- Debug CI #4 [Benjamin PIERRE]
- Debug CI #2 [Benjamin PIERRE]
- Debug CI [Benjamin PIERRE]
- Configure deploy key on gitlab CI [Benjamin PIERRE]
- Add missing yarn build instruction [Benjamin PIERRE]
- Add git to gitlab-ci [Benjamin PIERRE]
- Add bash to gitlab ci img [Benjamin PIERRE]
- Add git-directory-deploy to dev dependencies [Benjamin PIERRE]
- Begin to add deploy step on CI [Benjamin PIERRE]
- MAINT: Fix lint problems dues to babel-eslint old version : -
  https://github.com/eslint/eslint/issues/12117 -
  https://yarnpkg.com/cli/set/resolution - yarn set resolution babel-
  eslint@npm:10.0.1 ^10.1.0 [Benjamin PIERRE]
- Format code [Benjamin PIERRE]
- Add missing dependency for CI [Benjamin PIERRE]
- Update .gitlab-ci.yml file to run the linter #7 [Benjamin PIERRE]
- Add missing webpack plugin for CI [Benjamin PIERRE]
- Add webpack-cli and eslint to dev dependencies [Benjamin PIERRE]
- Add webpack as devdependency to check if it improves CI [Benjamin
  PIERRE]
- Update .gitlab-ci.yml file to run the linter #6 [PIERRE Benjamin]
- Update .gitlab-ci.yml file to run the linter #5 [PIERRE Benjamin]
- Update .gitlab-ci.yml file to run the linter #4 [PIERRE Benjamin]
- Update .gitlab-ci.yml file to run the linter #3 [PIERRE Benjamin]
- Update .gitlab-ci.yml file to run the linter #2 [PIERRE Benjamin]
- Update .gitlab-ci.yml file to run the linter [PIERRE Benjamin]
- Ensure that gitlab CI is active for this project [PIERRE Benjamin]
- Prepare for publish [Benjamin PIERRE]


v0.0.2 (2022-02-08)
-------------------

Features
~~~~~~~~
- Configure konnector run time interval between 2 and 8 am [Benjamin
  PIERRE]
- Add 1sec sleeps between all edf extranet calls in accordance with edf
  requirements [Benjamin PIERRE]
- Switch to node.js v16 and yarn v3 [Benjamin PIERRE]
- Last missing terms to rename [Benjamin PIERRE]
- Add missing changes on renaming [Benjamin PIERRE]
- Renamed "collectivity" in "community" [Benjamin PIERRE]
- Reconfigure logger to display dates types [Benjamin PIERRE]
- Add debug infos to determine how dates are passed to konnector in
  production [Benjamin PIERRE]
- Intent a date parsing from manifest.konnector [Benjamin PIERRE]
- Check what manifest.konnector "dates" fields type are in production
  [Benjamin PIERRE]
- Refactor code not to inherit from CookieKonnector class as it had an
  unsolvable problem in inheritance with dev and production mode
  regarding parameters init [Benjamin PIERRE]
- Configure the licence, manifest.konnector to prepare a first build
  [Benjamin PIERRE]
- Rewrite connector to have a more performant version, allowing it to be
  run with input configurable parameters [Benjamin PIERRE]

Bug fixes
~~~~~~~~~
- Date parsing for the production mode [Benjamin PIERRE]
- Broken dates selection from parameters [Benjamin PIERRE]

Maintenance
~~~~~~~~~~~
- Add bumpversion.cfg file [Benjamin PIERRE]
- Switch back version to 0.0.1 [Benjamin PIERRE]
- Update package version to 0.0.2 [Benjamin PIERRE]
- Update dev dependencies in package.json [Benjamin PIERRE]
- Test to mark fields as "advanced" in the field list declaration
  [Benjamin PIERRE]
- Improve connector fields definition in manifest [Benjamin PIERRE]
- Fix build dependencies [Benjamin PIERRE]
- Configure package.json file for a first deploy [Benjamin PIERRE]
- Remove unused file [Benjamin PIERRE]
- Update dependencies versions [Benjamin PIERRE]
- Define node version [Benjamin PIERRE]
- Remove unused file zombie.js [Benjamin PIERRE]

Other
~~~~~
- Revert "MAINT: Test to mark fields as "advanced" in the field list
  declaration" [Benjamin PIERRE]

  This reverts commit 3fb751cf67ddd32ae08205fd076999db1e595808

- Initial commit of initial version [Benjamin PIERRE]




.. Generated by gitchangelog
